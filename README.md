# Editorial
A based HTML5 Up "[Editorial](https://html5up.net/editorial)" theme, modified for [Pelican](http://getpelican.com).  

# Notes
* If you have questions => open an issue
* The theme is under dev, so don’t be suprised if it change
* I’ve made the mimum of modification of original Sass files. Every modifications/add are in separate file (custom.scss).
* The "static/css" are here to speedup the site generation, but if you change somme SCSS class, it must be deleted for regeneration.

# Feature
* Responsive Design (Desktop, Tablet, Tablet (Portrait), Mobile)
* No cloud font dependency, all in local
* FontAwesom 6 for all icons
* Sass file for easy modifications
* CSS  minification ( via [webassets](https://github.com/getpelican/pelican-plugins/tree/master/)
* Recent posts in sidebar
* Search bar (via [Search](https://github.com/pelican-plugins/search))
* Next & previous article (via [neighbors](https://github.com/getpelican/pelican-plugins/tree/master/)
* Comments via [StaticMan](https://staticman.net/) (comming …)

# Integration
* Matomo
* addthis (untested)
* Guages (untested)
* GoogleAnalytics (untested)
* Google TagManager (untested)
* Google Global Site Tag (untested)

All untested integration are copy/paste from [Flex](https://github.com/alexandrevicenzi/Flex) template.  
If you need another one, submit a PR.

# Plugins needed: 
* [neighbors](https://github.com/getpelican/pelican-plugins/tree/master/neighbors).  
* [webassets](https://github.com/getpelican/pelican-plugins/tree/master/assets). : You MUST have a Sass compiler  
* [Search](https://github.com/pelican-plugins/search) : You MUST have "cargo" (Rust packager). See the plugin documentation  
 
# Thanks to
* [Pelican](http://getpelican.com) of course and plugins contributors !
* [HTML5Up](https://html5up.net) for the template
* [Flex theme](https://github.com/alexandrevicenzi/Flex) for many inspiration and code

# Configuration needed
See the "pelicanconf.py.py-sample.py" for a complete exemple

| Key |Optional ? | Value |
|-----|-----------|-------|
| SOCIAL | Y | A list of tuples (brand,link). The brand is used to match the Awesome bran icon. Its appear on the sidebar to to allow your readers to follow you|
| SHARE | Y | LA list of tuples (brand,link) like SOCIAL. Buttons on the right top of article to share it. By default there a email button|
| ABOUT | Y | Text appear in the side bar section «Get in Touch», above the list on "CONTACT"|
| CONTACT | Y | A list of tuples (icon,text), who appear in the side bar. You can put an HTML tag for exemple "mailto"|
| MAMOMO_URL & MATOMO_SITE_ID | Y | Only needed if you use Matomo server. No need "https://" for the URL|
| SEARCH_MODE | Y | Only needed if you use Stork for search engine|

# Capture d’écran
![Screenshot-Desktop](Screenshot-Desktop.png)
