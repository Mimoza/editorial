#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = ''
SITENAME = ''
SITEURL = ''

PLUGINS = ['neighbors','webassets']
THEME = 'editorial'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_DOMAIN = SITEURL
FEED_ATOM = 'feeds/atom.xml'
FEED_RSS = 'feeds/rss.xml'
FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ALL_RSS = 'feeds/all.rss.xml'
CATEGORY_FEED_ATOM = None
CATEGORY_FEED_RSS = None
TRANSLATION_FEED_ATOM = None
TRANSLATION_FEED_RSS = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),
)

# Social widget
SOCIAL = (
    ('twitter', 'https://twitter.com/'),
    ('linkedin', 'https://www.linkedin.com'),
    ('github', 'https://github.com/'),
    ('gitlab', 'https://gitlab.com/'),
    ('facebook', 'https://facebook.com'),
    ('instagram', 'https://instagram.com')
)

# Article share widgets
SHARE = (
    ("twitter", "https://twitter.com/intent/tweet/?text=Features&amp;url="),
    ("linkedin", "https://www.linkedin.com/sharing/share-offsite/?url="),
    ("reddit", "https://reddit.com/submit?url="),
    ("facebook", "https://facebook.com/sharer/sharer.php?u="),
    ("whatsapp", "https://api.whatsapp.com/send?text=Features - "),
    ("telegram", "https://telegram.me/share/url?text=Features&amp;url="),
)

DEFAULT_PAGINATION = 20

# Pour générer automatiquement un résumé a afficher
SUMMARY_MAX_LENGTH = 50
SUMMARY_END_SUFFIX = '…'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
#DELETE_OUTPUT_DIRECTORY = True

ABOUT = ''
CONTACT = (
    ('envelope', '<a href="mailto:contact@domaine.tld?Subject=Contact&Body=Bonjour,%0AJe%20voudrais%20en%20apprendre%20plus%20sur%20votre%20blog">contact@domaine.tld</a>'),
    ('phone','+01 345-345-345'),
    ('home', '42 rue de l’oublie, Moncug')
)
